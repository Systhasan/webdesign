from django.shortcuts import render
from django.http import HttpResponse
from .models import Product
from .forms import ProductForm, RawProduct


# Create your views here.
def home_view(request, *args, **kwargs):
    obj = Product.objects.get(id=1)
    my_context = {
        'title': obj.title,
        'des': obj.description
    }
    return render(request, 'home/index.html', my_context)


def add_product(request):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    '''This render used for print input field by default.'''
    return render(request, 'home/add_product.html', context)


def add_other_product(request):
    print(request.POST)
    if request.method == "POST":
        title = request.POST.get('title')
        print(title)

    context = {}
    return render(request, 'home/add_product_view.html', context)


def add_another_product(request):
    my_form = RawProduct()

    # if form are post request.
    if request.method == "POST":
        my_form = RawProduct(request.POST)
        if my_form.is_valid():
            Product.objects.create(**my_form.cleaned_data)
    # Return Data.
    context = {
        "form": my_form
    }
    return render(request, 'home/add_another_product.html', context)


def test_view(request, *args, **kwargs):
    print(request.user)
    return HttpResponse("<h1>Demo Test</h1>")
