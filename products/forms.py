from django import forms
from .models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            'title',
            'description',
            'price',
            'summary'
        ]


class RawProduct(forms.Form):
    title = forms.CharField()
    description = forms.CharField()
    price = forms.CharField()
    summary = forms.CharField()