from django.contrib import admin
from django.urls import path

'''
@app_name products
@desc load view method form products app 
'''
from products.views import home_view, add_product, add_other_product, add_another_product


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', home_view, name="home"),
    path('add-product/', add_product, name="add_product"),
    path('add-other-product/', add_other_product, name="add_other_product"),
    path('validateWayToAddProduct/', add_another_product, name="validateWayToAddProduct"),
    path('', home_view, name="home")
]
